package p0;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

//	private String user;
//	private String password;
//	private long id;
//	//private String name;
//	//private String email;
//	//private double depLimit;
//	//private double witLimit;
//	private byte type;
//	private LinkedList<Long> accts;
	Customer tester;
	Customer tester2;
	LinkedList<Long> link;
	LinkedList<Long> link2;
	@Before
	public void initializations()
	{
		link = new LinkedList<Long>();
		link2 = new LinkedList<Long>();
		link2.add((long)6969);
		link2.add((long)420);
		link2.add((long)1337);
		link2.add((long)911);
		tester = new Customer("Allen", "Keys", (byte)1, (long)9911);
		tester2 = new Customer("Philips", "Head", (byte)2, (long)1199, link2);
	}
	@Test
	public void defaultCustomerTest() 
	{
		//initialization
		assertTrue(tester.getUser().equals("Allen"));
		assertTrue(tester.getPassword().equals("Keys"));
		assertTrue(tester.getID()==9911);
		assertTrue(tester.getType()==1);
		assertTrue(tester.getAccts().equals(link));
		assertTrue(tester.getAccts().equals(link));
		tester.changePassword("Ni");
		assertTrue(tester.getPassword().equals("Ni"));
		//2nd
		assertTrue(tester2.getUser().equals("Philips"));
		assertTrue(tester2.getPassword().equals("Head"));
		assertTrue(tester2.getID()==1199);
		assertTrue(tester2.getType()==2);
		assertTrue(tester2.getAccts().equals(link2));
		assertTrue(tester2.getAccts().equals(link2));
		tester2.changePassword("Dutch");
		assertTrue(tester2.getPassword().equals("Dutch"));
	}
	@Test
	public void addAcctTest() 
	{
		assertTrue(tester.addAcct((long)9001));
		assertTrue(tester.getAccts().contains((long)9001));
		assertFalse(tester.addAcct((long)9001));
		assertTrue(tester2.addAcct((long)9001));
		assertTrue(tester2.getAccts().contains((long)9001));
		assertFalse(tester2.addAcct((long)9001));
	}
	@Test
	public void removeAcctTest() 
	{
		assertFalse(tester.removeAcct((long)9001));
		assertTrue(tester.addAcct((long)9001));
		assertTrue(tester.getAccts().contains((long)9001));
		assertTrue(tester.removeAcct((long)9001));
		assertFalse(tester.getAccts().contains((long)9001));
		assertFalse(tester2.removeAcct((long)9001));
		assertTrue(tester2.addAcct((long)9001));
		assertTrue(tester2.getAccts().contains((long)9001));
		assertTrue(tester2.removeAcct((long)9001));
		assertFalse(tester2.getAccts().contains((long)9001));
	}
	@Test
	public void toStringTest()
	{
		System.out.println(tester);
		System.out.println(tester2);
		assertTrue(tester.toString().equals("Allen\nKeys\n1\n9911\n0\n"));
		assertTrue(tester2.toString().equals("Philips\nHead\n2\n1199\n4\n6969\n420\n1337\n911"));
	}
}
