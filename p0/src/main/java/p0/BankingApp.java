package p0;

//import java.io.BufferedWriter;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author dylan xu
 * banking app simulation for revature
 *
 */
public class BankingApp {
	
	private static String customerFile = "customers";
	private static String acctFile = "accounts";
	private static String idsFile = "IDs";
	
	private static String printUserData(String user,HashMap<String,Customer> utc) throws AccountDoesNotExistException
	{
		String s = "";
		int i = 0;
		if(utc.containsKey(user))
		{
			s+="Username: " + utc.get(user).getUser() + "\n";
			s+="Password: " + utc.get(user).getPassword() + "\n";
			s+="Type: ";
			switch(utc.get(user).getType())
			{
				case 0:
					s+="customer\n";
					break;
				case 1:
					s+="employee\n";
					break;
				case 2:
					s+="admin\n";
					break;
				
			}
			s+="ID: " + utc.get(user).getID() + "\n";
			s+="This user has " + utc.get(user).getAccts().size() + " accounts\n";
			for(; i<utc.get(user).getAccts().size()-1;i++)
			{
				s+=utc.get(user).getAccts().get(i).toString()+"\n";
			}
			if(utc.get(user).getAccts().size()!=0)
			{
				s+=utc.get(user).getAccts().get(i).toString();
			}
		}
		else
		{
			throw new AccountDoesNotExistException("Account not found");
		}
		return s;
	}
	private static String printAccountData(Long l,HashMap<Long,Account> nta) throws AccountDoesNotExistException
	{
		String s = "";
		int i = 0;
		if(nta.containsKey(l))
		{
			s+="Account Number: " + nta.get(l).getAcctNumber()+ "\n";
			s+="Account Holdings:" + nta.get(l).getAcctHoldings() + "\n";
			s+="Status: ";
			switch(nta.get(l).getStatus())
			{
				case 0:
					s+="Pending\n";
					break;
				case 1:
					s+="Accepted\n";
					break;
				case 2:
					s+="Rejected\n";
					break;
				
			}
			s+="Type: ";
			if(nta.get(l).getType())
			{
				s+= "Joint\n";
			}
			else
			{
				s+= "Personal\n";
			}
			s+="Deposit Limit: " + nta.get(l).getDepLimit() + "\n";
			s+="Withdrawal Limit: " + nta.get(l).getWitLimit() + "\n";
			s+="This account has " + nta.get(l).getOwners().length + " owner(s)\n";
			for(; i<nta.get(l).getOwners().length-1;i++)
			{
				s+=nta.get(l).getOwners()[i]+"\n";
			}
			if(nta.get(l).getOwners().length!=0)
			{
				s+=nta.get(l).getOwners()[i];
			}
		}
		else
		{
			throw new AccountDoesNotExistException("Bank account not found");
		}
		return s;
	}
	/**
	 * @param u Customer requesting
	 * @param a source account number (the one that's losing the money)
	 * @param b destination account number (the one that's getting the money)
	 * @param amount self explanatory
	 * @return if the withdrawal was successful
	 * @throws AccountDoesNotExistException
	 */
	private static boolean transfers(Customer u, Long a, Long b,double amount, HashMap<Long,Account> nta) throws AccountDoesNotExistException
	{
		//check if the user is an admin or owns the account
		if(u.getAccts().contains(a) && (u.getType() != 2))
		{
			throw new AccountDoesNotExistException("You don't own this account");
		}
		if(!nta.containsKey(a))
		{
			throw new AccountDoesNotExistException("Source account not found");
		}
		if(!nta.containsKey(b))
		{
			throw new AccountDoesNotExistException("Destination account not found");
		}
		if((nta.get(a).getStatus()==0))
		{
			throw new AccountDoesNotExistException("Source account pending");
		}
		if((nta.get(b).getStatus()==0))
		{
			throw new AccountDoesNotExistException("Destination account pending");
		}if((nta.get(a).getStatus()==2))
		{
			throw new AccountDoesNotExistException("Source account rejected");
		}
		if((nta.get(b).getStatus()==2))
		{
			throw new AccountDoesNotExistException("Destination account rejected");
		}
		
		if(a.equals(b)||nta.get(a).getAcctHoldings()<amount)
		{
			return false;
		}
		nta.get(a).withdraw(amount);
		nta.get(b).deposit(amount);
		//log transfer
		return true;
	}
	/**
	 * @param u customer requestiong withdrawal
	 * @param a account number
	 * @param amount self explanatory
	 * @return if the withdrawal was successful
	 * @throws AccountDoesNotExistException
	 */
	private static boolean depo(Customer u, Long a,double amount, HashMap<Long,Account> nta) throws AccountDoesNotExistException
	{
		if(!nta.containsKey(a))
		{
			throw new AccountDoesNotExistException("Account does not exist");
		}
		if((!u.getAccts().contains(a))&&(u.getType()!=2))
		{
			throw new AccountDoesNotExistException("You don't own this account");
		}
		if((nta.get(a).getStatus()==0))
		{
			throw new AccountDoesNotExistException("Account pending");
		}if((nta.get(a).getStatus()==2))
		{
			throw new AccountDoesNotExistException("Account rejected");
		}
		if((nta.get(a).getDepLimit()<amount)&&(nta.get(a).getDepLimit()>0)&&(u.getType()!=2))
		{
			return false;
		}
		nta.get(a).deposit(amount);
		//log transfer and keep track of limit for the day
		return true;
	}
	/**
	 * @param u customer requestiong withdrawal
	 * @param a account number
	 * @param amount self explanatory
	 * @return if the withdrawal was successful
	 * @throws AccountDoesNotExistException
	 */
	private static boolean with(Customer u, Long a,double amount, HashMap<Long,Account> nta) throws AccountDoesNotExistException
	{
		if(!nta.containsKey(a))
		{
			throw new AccountDoesNotExistException("Account does not exist");
		}
		if(!u.getAccts().contains(a)&&(u.getType()!=2))
		{
			throw new AccountDoesNotExistException("You don't own this account");
		}
		if((nta.get(a).getStatus()==0))
		{
			throw new AccountDoesNotExistException("Account pending");
		}if((nta.get(a).getStatus()==2))
		{
			throw new AccountDoesNotExistException("Account rejected");
		}
		if((nta.get(a).getAcctHoldings()<amount) || ((nta.get(a).getWitLimit()<amount)&&(nta.get(a).getWitLimit()>0)))
		{
			return false;
		}
		nta.get(a).withdraw(amount);
		//log transfer and keep track of limit for the day
		return true;
	}
	/**
	 * @param username self explanatory
	 * @param password self explanatory
	 * @param utc username to customer hashmap
	 * @return if login successful
	 * @throws LoginException 
	 */
	private static boolean login(String username, String password, HashMap<String, Customer> utc) throws LoginException
	{
		if(!(utc.containsKey(username)))
		{
			throw new LoginException("Username not found");
		}
		if(!(utc.get(username).getPassword().equals(password)))
		{
			throw new LoginException("Incorrect Password");
		}
		return true;
	}
	/**
	 * @param username  self explanatory
	 * @param password  self explanatory
	 * @param type 0 = customer, 1 = employee, 2 = admin
	 * @param id employeeID, if type = 0, id should be -1
	 * @return returns whether the registration is successful
	 * @throws UsernameUsedException 
	 */
	private static boolean register(String username, String password, byte type, long id, HashMap<String,Customer> utc, LinkedList<Long> eid) throws UsernameUsedException
	{
		//boolean used to see if id has been used
		if(utc.containsKey(username))
		{
			throw new UsernameUsedException("This username has already been used");
		}
		if(type!=0)
		{
			//if the employee ID doesn't exist in the database
			if((!eid.contains(id)))
			{
				return false;//throw error
			}
			//checking if any user has used employee id before
			for(Object str : utc.keySet().toArray())
			{
				if(utc.get((String)str).getID()==id)
				{
					throw new UsernameUsedException("This ID has already been used");
				}
			}
		}
		Customer user = new Customer(username, password, type, id);
		utc.put(username, user);
		return true;
	}
	private static boolean saveAccounts(String fileName, HashMap<Long,Account> accountList)
	{
		try 
		{
			FileOutputStream fileOut =new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(accountList);
			out.close();
			fileOut.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	private static boolean saveCustomers(String fileName, HashMap<String,Customer> customerList)
	{
		try 
		{
			FileOutputStream fileOut =new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(customerList);
			out.close();
			fileOut.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	private static boolean saveEmployeeIDs(String fileName, LinkedList<Long> ids)
	{
		try 
		{
			FileOutputStream fileOut =new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(ids);
			out.close();
			fileOut.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return true;
	}
	@SuppressWarnings("unchecked")
	private static HashMap<Long,Account> getAccounts(String fileName) throws Exception
	{
		try 
		{
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			HashMap<Long,Account> nta = (HashMap<Long,Account>) in.readObject();
			in.close();
			fileIn.close();
			return nta;
		} 
		catch (IOException e) 
		{
			throw e;
		} 
		catch (ClassNotFoundException c) 
		{
			throw c;
		}
	}
	@SuppressWarnings("unchecked")
	private static HashMap<String,Customer> getCustomers(String fileName) throws Exception
	{
		try 
		{
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			HashMap<String,Customer> utc = (HashMap<String,Customer>) in.readObject();
			in.close();
			fileIn.close();
			return utc;
		} 
		catch (IOException e) 
		{
			throw e;
		} 
		catch (ClassNotFoundException c) 
		{
			throw c;
		}
	}
	@SuppressWarnings("unchecked")
	private static LinkedList<Long> getEmployeeIDs(String fileName) throws Exception
	{
		try 
		{
			FileInputStream fileIn = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			LinkedList<Long> utc = (LinkedList<Long>) in.readObject();
			in.close();
			fileIn.close();
			return utc;
		} 
		catch (IOException e) 
		{
			throw e;
		} 
		catch (ClassNotFoundException c) 
		{
			throw c;
		}
	}
	/*private static boolean saveAccounts(String fileName, HashMap<Long,Account> accountList) throws IOException
	{
		BufferedWriter bw;
		bw = new BufferedWriter(new FileWriter(fileName));
		int i = 0;
		Object[] arr = (accountList.keySet().toArray());
		try 
		{
			for(; i<arr.length-1;i++ )
			{
				bw.write(accountList.get((Long)arr[i]).toString()+"\n");
			}
			if(accountList.size()!=0)
			{
				bw.write(accountList.get((Long)arr[i]).toString());
			}
		}
		catch(IOException e)
		{
			bw.close();
			throw e;
		}
		bw.close();
		return true;
	}
	
	private static boolean saveCustomers(String fileName, HashMap<String,Customer> customerList) throws IOException
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		int i = 0;
		Object[] arr = customerList.keySet().toArray();
		try
		{
			for(; i<customerList.size()-1;i++ )
			{
				bw.write(customerList.get((String)arr[i]).toString()+"\n");
			}
			if(customerList.size()!=0)
			{
				bw.write(customerList.get((String)arr[i]).toString());
			}
		}
		catch(IOException e)
		{
			bw.close();
			throw e;
		}
		bw.close();
		return true;
	}
	private static boolean saveEmployeeIDs(String fileName, LinkedList<Long> ids) throws IOException
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		int i = 0;
		try
		{
			for(; i<ids.size()-1;i++ )
			{
				bw.write(ids.get(i).toString()+"\n");
			}
			if(ids.size()!=0)
			{
				bw.write(ids.get(i).toString());
			}
		}
		catch(IOException e)
		{
			bw.close();
			throw e;
		}
		bw.close();
		return true;
	}
	//attempts to load an HashMap from a file, if a FileNotFoundException is thrown, 
	//notify the end user that there ain't a file(this is either a new bank or a lost file)
	//as a warning, else, if it throws a different error due to parsing, notify the end
	//user by telling them 
	private static HashMap<Long,Account> getAccounts(String fileName) throws IOException
	{
		HashMap<Long,Account>  accounts = new HashMap<Long,Account> ();
		//fr = new FileReader(fileName);
		BufferedReader br;
		try 
		{
			br = new BufferedReader(new FileReader(fileName));
		}
		catch(FileNotFoundException e)
		{
			//probably actually throw this specific exception instead of just IO
			throw e;
		}
		String s;
		int i,j;
		long acctNumber;
		double acctHoldings;
		byte status;//0 = pending, 1 = accepted, 2 = rejected
		boolean type;//joint = true, solo = false 
		double depLimit;//neg = no limit
		double witLimit;//neg = no limit
		String[] owner;//name of owner(s)?
		while(((s = br.readLine())!=null) && (!s.equals("")))
		{
			acctNumber = Long.parseLong(s);
			s = br.readLine();
			acctHoldings = Double.parseDouble(s);
			s = br.readLine();
			status = Byte.parseByte(s);
			s = br.readLine();
			depLimit = Double.parseDouble(s);
			s = br.readLine();
			witLimit = Double.parseDouble(s);
			s = br.readLine();
			type = Boolean.parseBoolean(s);
			s = br.readLine();
			i = Integer.parseInt(s);
			owner = new String[i];
			for(j = 0; j<i; j++)
			{
				s = br.readLine();
				owner[j] = s;
			}
			accounts.put(acctNumber, new Account(acctNumber,acctHoldings, status, depLimit, witLimit, owner));
		}
		br.close();
		return accounts;
	}
	private static HashMap<String,Customer> getCustomers(String fileName) throws IOException
	{
		HashMap<String,Customer>  customers = new HashMap<String,Customer>();
		//fr = new FileReader(fileName);
		BufferedReader br;
		try 
		{
			br = new BufferedReader(new FileReader(fileName));
		}
		catch(FileNotFoundException e)
		{
			//probably actually throw this specific exception instead of just IO
			throw e;
		}
		String s;
		String user;
		String password;
//		double depLimit;
//		double witLimit;
		byte type;
		long id;
		int size;
		int i;
		LinkedList<Long> ll;
		while(((s = br.readLine())!=null) && (!s.equals("")))
		{
			//something to help with identifying who's an employee... 
			//possible revisions to customer data type
			//maybe just a different list?
			ll = new LinkedList<Long>();
			user = s;
			s = br.readLine();
			//System.out.println("User: " + user);//used for testing/getting insight
			password = s;
			//System.out.println("pswd: " + s);
//			s = br.readLine();
//			depLimit = Double.parseDouble(s);
//			s = br.readLine();
//			witLimit = Double.parseDouble(s);
			s = br.readLine();
			type = Byte.parseByte(s);
			s = br.readLine();
			id = Long.parseLong(s);
			s = br.readLine();
			size = Integer.parseInt(s);
			for(i = 0; i<size; i++)
			{
				s = br.readLine();
				ll.add(Long.parseLong(s));
			}
			customers.put(user,new Customer(user,password,//depLimit,witLimit,
							type,id,ll));
		}
		br.close();
		return customers;
	}
	private static LinkedList<Long> getEmployeeIDs(String fileName) throws IOException
	{
		//acceptable ids
		LinkedList<Long> ids = new LinkedList<Long>();
		BufferedReader br;
		try 
		{
			br = new BufferedReader(new FileReader(fileName));
		}
		catch(FileNotFoundException e)
		{
			//probably actually throw this specific exception
			throw e;
		}
		String s;
		while(((s = br.readLine())!=null) && (!s.equals("")))
		{
			ids.add(Long.parseLong(s));
		}
		br.close();
		return ids;
	}*/
	private static boolean requestAccount(Long acctNum, String[] own, Customer u,HashMap<Long,Account> nta)
	{
		if(nta.containsKey(acctNum))
		{
			return false;
		}
		Account act = new Account(acctNum,own);
		nta.put(acctNum, act);
		u.addAcct(acctNum);
		return true;
	}
	
	
	public static void main(String[] args) throws IOException
	{
		HashMap<String,Customer> userToCustom;//username to customer hashmap
		HashMap<Long,Account> numberToAccts;//acctID to accts hashmap
		LinkedList<Long> employeeIDs;//employeeID data holder
		Customer cust;//customer holder after login
		File temp;//used if file doesn't exist, create it
		Scanner s = new Scanner(System.in);//scanner for inputs
		String user;//username holder
		String pass;//password holder
		double doub;//double holder, used for $ amounts
		String[] owners;//username array for joint account registration
		boolean contains;//contains used for joint accounts to find if the username array actually contains the username
		Long acctNum;//used to keep track of which is the last acct, we go in order here
		char c;//used for scanning characters
		Long l;//long scanner
		Long l2;//2nd long scanner
		int b = 0;//used for scanning numbers
		byte state = 0;//used for keeping track of the state for the sm
		System.out.println("Loading Bank App Booting sequence");
		
		try
		{
			userToCustom = getCustomers(customerFile);
		}
		catch(IOException e) 
		{
			do 
			{
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Missing customers file, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				userToCustom = new HashMap<String,Customer>();
				temp = new File(customerFile);
				if(temp.createNewFile())
				{
					System.out.println("Customer file created");
				}
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
		}
		//catch(NumberFormatException e)
		catch(Exception e)
		{
			do 
			{
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Files corrupted, overwriting previous data, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				userToCustom = new HashMap<String,Customer>();
				temp = new File(customerFile);
				if(!temp.createNewFile())
				{
					temp.delete();
					temp.createNewFile();
				}
				System.out.println("Customer file created");
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
		}
		try {
			numberToAccts = getAccounts(acctFile);
		}
		catch(IOException e) 
		{
			do {
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Missing accounts file, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				numberToAccts = new HashMap<Long,Account>();
				temp = new File(acctFile);
				if(temp.createNewFile())
				{
					System.out.println("Account file created");
				}
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
		}
		//catch(NumberFormatException e)
		catch(Exception e)
		{
			do 
			{
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Files corrupted, overwriting previous data, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				numberToAccts = new HashMap<Long,Account>();
				temp = new File(acctFile);
				if(!temp.createNewFile())
				{
					temp.delete();
					temp.createNewFile();
				}
					System.out.println("Account file created");
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
		}
		try {
			employeeIDs = getEmployeeIDs(idsFile);
		}
		catch(IOException e) 
		{
			do {
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Missing employee IDs file, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				employeeIDs = new LinkedList<Long>();
				//default new id for admin to be possible
				employeeIDs.add((long) 0);
				temp = new File(idsFile);
				if(!temp.createNewFile())
				{
					temp.delete();
					temp.createNewFile();
				}
					System.out.println("ID file created");
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
			
		}
		//catch(NumberFormatException e)
		catch(Exception e)
		{
			do 
			{
				//warn user of missing files, if they continue, create a new thing
				System.out.println("Files corrupted, overwriting previous data, continue?");
				System.out.println("(Y)es   (N)o");
				c = s.nextLine().charAt(0);
				c = Character.toLowerCase(c);
			}while((c != 'y') &&(c != 'n'));
			if(c == 'y')
			{
				employeeIDs = new LinkedList<Long>();
				temp = new File(idsFile);
				if(temp.createNewFile())
				{
					System.out.println("ID file created");
				}
			}
			else
			{
				s.close();
				System.out.println("Thank you, goodbye.");
				return;
			}
		}
		System.out.println("Loading Successful!");
		acctNum = (long) numberToAccts.size();
		state = 1;
		while(true) {
			System.out.println("Welcome to the bank, how may we help you?");
			//state: initial login screen, if successful sign in, switch to state 2, else remain
			switch(state)
			{
				case 1:
				{
					do 
					{
						System.out.println("1. Sign in");
						System.out.println("2. Register Customer Account");
						System.out.println("3. Register Employee Account");
						System.out.println("4. Register Admin Account");
						System.out.println("5. Quit");
						try {
							b = s.nextInt();
							s.nextLine();
						}
						catch(InputMismatchException e)
						{
							b = 0;
							s.nextLine();
						}
						if(b>5||b<1)
						{
							System.out.println("Please enter an integer between 1 and 5");
						}
					}while(b>5||b<1);
					switch(b)
					{
						case 1:
							state = 2;
							break;
						case 2:
							state = 3;
							break;
						case 3:
							state = 3;
							break;
						case 4:
							state = 3;
							break;
						case 5:
							state = 0;
							break;
						default:
							break;
					}
					break;
				}
				//login screen
				case 2:
				{
					System.out.println("Please enter your username");
					user = s.nextLine();
					System.out.println("Please enter your password");
					pass = s.nextLine();
					try {
						if(login(user, pass, userToCustom))
						{
							System.out.println("Login Successful!");
							//state = 4;
							cust = userToCustom.get(user);
							do {
								do {
									System.out.println("Hello " + cust.getUser());
									System.out.println("What would you like to do?");
									System.out.println("1. List accounts and holdings");
									System.out.println("2. Deposit into an account");
									System.out.println("3. Withdraw from an account");
									System.out.println("4. Transfer from an account into another");
									System.out.println("5. Change password");
									System.out.println("6. Create new bank account");
									System.out.println("7: Log out");
									//System.out.println("x. List logs");
									if(cust.getType()>=1)
									{
										System.out.println("8. List pending accounts");
										System.out.println("9. Approve/Reject accounts");
										System.out.println("10. List all user accounts");
										System.out.println("11. List all bank accounts");
										System.out.println("12. List user account info");
										System.out.println("13. List bank account info");
									}
									if(cust.getType()==2)
									{
										System.out.println("14. Create an employee ID");
										System.out.println("15. Delete bank account");
									}
									try 
									{
										b = s.nextInt();
										s.nextLine();
									}
									catch(InputMismatchException e)
									{
										b = 0;
										s.nextLine();
									}
									if((b>7||b<1) && cust.getType()==0)
									{
										System.out.println("Please enter an integer between 1 and 7");
									}
									if((b>13||b<1) && cust.getType()==1)
									{
										System.out.println("Please enter an integer between 1 and 13");
									}
									if((b>15||b<1) && cust.getType()==2)
									{
										System.out.println("Please enter an integer between 1 and 15");
									}
								}while(b<1||b>15);
								switch(b)
								{
									case 1:
									{
										if(cust.getAccts().size() == 0)
										{
											System.out.println("You don't have any accounts!");
										}
										else
										{
												for(Long acct:cust.getAccts())
												{
													try 
													{
														System.out.println(printAccountData(acct, numberToAccts));
													}
													catch(AccountDoesNotExistException e)
													{
														System.out.println(e.getMessage() + "Account is: " + acct);
													}
												}
											
										}
										break;
									}
									case 2:
									{
										System.out.println("Which account would you like to deposit into?");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if(l<0 || (!cust.getAccts().contains(l)&&cust.getType()!=2))
										{
											System.out.println("Please enter an actual account number you own");
										}
										else
										{
											System.out.println("Please enter the amount you would like to deposit");
											try 
											{
												doub = s.nextDouble();
												s.nextLine();
											}
											catch(InputMismatchException e)
											{
												doub = -1;
												s.nextLine();
											}
											if(doub>0)
											{
												try 
												{
													depo(cust, l, doub, numberToAccts);
													System.out.println("Deposit Successful");
												} 
												catch (AccountDoesNotExistException e) 
												{
													System.out.println(e.getMessage());
													break;
												}
											}
											else
											{
												System.out.println("Please enter a positive number");
											}
										}
										break;
									}
									case 3:
									{
										System.out.println("Which account would you like to withdraw from?");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if(l<0 || !cust.getAccts().contains(l))
										{
											System.out.println("Please enter an actual account number you own");
										}
										else
										{
											System.out.println("Please enter the amount you would like to withdraw");
											try 
											{
												doub = s.nextDouble();
												s.nextLine();
											}
											catch(InputMismatchException e)
											{
												doub = -1;
												s.nextLine();
											}
											if(doub>0)
											{
												try 
												{
													if(with(cust, l, doub, numberToAccts))
													{
														System.out.println("Withdraw Successful");
													}
													else
													{
														System.out.println("Withdraw Failed");
													}
												} 
												catch (AccountDoesNotExistException e)
												{
													System.out.println(e.getMessage());
													break;
												}
											}
											else
											{
												System.out.println("Please enter a positive number");
											}
										}
										break;
									}
									case 4:
									{
										System.out.println("Which account would you like to transfer from?");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if(l<0||!numberToAccts.containsKey(l))
										{
											System.out.println("Please enter an actual account number");
										}
										else
										{
											System.out.println("Which account would you like to transfer to?");
											try 
											{
												l2 = s.nextLong();
												s.nextLine();
											}
											catch(InputMismatchException e)
											{
												l2 = (long) -1;
												s.nextLine();
											}
											if(l2<0||!numberToAccts.containsKey(l2))
											{
												System.out.println("Please enter an actual account number");
											}
											else
											{
												System.out.println("Please enter the amount you would like to transfer");
												try 
												{
													doub = s.nextDouble();
													s.nextLine();
												}
												catch(InputMismatchException e)
												{
													doub = -1;
													s.nextLine();
												}
												if(doub>0)
												{
													try 
													{
														if(transfers(cust, l, l2, doub, numberToAccts))
														{
															System.out.println("Transfer Successful");
														}
														else
														{
															System.out.println("Transfer Failed");
														}
													} 
													catch (AccountDoesNotExistException e)
													{
														System.out.println(e.getMessage());
														break;
													}
												}
												else
												{
													System.out.println("Please enter a positive number");
												}
											}
										}
										break;
									}
									case 5:
									{
										System.out.println("Please enter your new password");
										if(cust.changePassword(s.nextLine()))
										{
											System.out.println("Password changed");
										}
										else
										{
											System.out.println("Password not changed");
										}
										break;
									}
									case 6:
									{
										System.out.println("This account will be a");
										System.out.println("1.Joint Account");
										System.out.println("2.Personal Account");
										try 
										{
											b = s.nextInt();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											b = -1;
											s.nextLine();
										}
										if(b==1)
										{
											System.out.println("How many other users will this joint account have?");
											try 
											{
												b = s.nextInt();
												s.nextLine();
											}
											catch(InputMismatchException e)
											{
												b = -1;
												s.nextLine();
											}
											if(b<1)
											{
												System.out.println("If you aren't registering an account for more than");
												System.out.println("1 person, create a personal account instead");
											}
											else
											{
												owners = new String[b+1];
												owners[0] = cust.getUser();
												contains = true;
												for(int i = 1; i<b+1; i++)
												{
													System.out.print("Please provide the username of the "+(i+1));
													if((((i+1)%100>20)||((i+1)%100<10))&&((i+1)%10==2))
													{
														System.out.print("nd");
													}
													else if((((i+1)%100>20)||((i+1)%100<10))&&((i+1)%10==4))
													{
														System.out.print("rd");
													}
													else
													{
														System.out.print("th");
													}
													System.out.println(" owner");
													owners[i] = s.nextLine();
													if(!userToCustom.containsKey(owners[i]))
													{
														contains = false;
														System.out.println("Username not found");
														break;
													}
													if(owners[i].equals(cust.getUser()))
													{
														contains = false;
														System.out.println("You can't enter your own name as a separate user");
														break;
													}
												}
												if(requestAccount(acctNum, owners, cust, numberToAccts)&&contains)
												{
													for(int i = 1; i< owners.length;i++)
													{
														userToCustom.get(owners[i]).addAcct(acctNum);
													}
													acctNum++;
													System.out.println("Account requested");
												}
												else
												{
													System.out.println("Account request failed");
												}
											}
										}
										else if(b == 2)
										{
											owners = new String[1];
											owners[0] = cust.getUser();
											if(requestAccount(acctNum, owners, cust, numberToAccts))
											{
												acctNum++;
												System.out.println("Account requested");
											}
											else
											{
												System.out.println("Account request failed");
											}
										}
										else 
										{
											System.out.println("Please enter either 1 or 2");
										}
										break;
									}
									case 7:
									{
										System.out.println("Thank you for using our service " + cust.getUser());
										state = 1;
										break;
									}
									case 8:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("The following accounts are pending");
										for(Object i : numberToAccts.keySet().toArray())
										{
											if(numberToAccts.get(i).getStatus() == 0)
											{
												System.out.println(""+ (Long)i);
											}
										}
										break;
									}
									case 9:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("Please enter the account you would like to approve/reject");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if((l<0) || (!numberToAccts.containsKey(l)) || ((numberToAccts.get(l).getStatus()!=0) && (cust.getType()==1)))
										{
											System.out.println("Please enter a valid account");
										}
										else
										{
											System.out.println("Would you like to set the status of the account to");
											System.out.println("1. Accepted");
											System.out.println("2. Rejected");
											if(cust.getType()==2)
											{
												System.out.println("3. Pending");
											}
											try 
											{
												b = s.nextInt();
												s.nextLine();
											}
											catch(InputMismatchException e)
											{
												b = -1;
												s.nextLine();
											}
											if(b==1)
											{
												numberToAccts.get(l).setStatus((byte)1);
												System.out.println("Account Accepted");
											}
											else if (b == 2)
											{
												numberToAccts.get(l).setStatus((byte)2);
												System.out.println("Account Rejected");
											}
											else if(b==3&&cust.getType()==2)
											{
												numberToAccts.get(l).setStatus((byte)0);
												System.out.println("Account set to pending");
											}
											else
											{
												System.out.println("Please enter either 1 or 2 to accept or reject");
												if(cust.getType()==2)
												{
													System.out.println("or 0 to set to pending");
												}
											}
										}
										break;
									}
									case 10:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("The following usernames are on file");
										for(Object str:userToCustom.keySet().toArray())
										{
											System.out.println((String)str);
										}
										break;
									}
									case 11:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("The following accounts are on file");
										for(Object str:numberToAccts.keySet().toArray())
										{
											System.out.println((Long)str);
										}
										break;
									}
									case 12:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("Which user's info would you like to print?");
										try 
										{
											System.out.println(printUserData(s.nextLine(),userToCustom));
										} 
										catch (AccountDoesNotExistException e) 
										{
											System.out.println(e.getMessage());
										}
										break;
									}
									case 13:
									{
										if(cust.getType()==0)
										{
											break;
										}
										System.out.println("Which account's info would you like to print?");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long)-1;
											s.nextLine();
										}
										try 
										{
											System.out.println(printAccountData(l,numberToAccts));
										} 
										catch (AccountDoesNotExistException e) 
										{
											System.out.println(e.getMessage());
										}
										break;
									}
									case 14:
									{
										if(cust.getType()!=2)
										{
											break;
										}
										System.out.println("Please enter the new employee ID");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if(l<0)
										{
											System.out.println("Please enter the a valid employee ID");
										}
										else if(employeeIDs.contains(l))
										{
											System.out.println("ID already exists");
										}
										else
										{
											employeeIDs.add(l);
										}
										break;
									}
									case 15:
									{
										if(cust.getType()!=2)
										{
											break;
										}
										System.out.println("Please enter bank account number you wish to delete");
										try 
										{
											l = s.nextLong();
											s.nextLine();
										}
										catch(InputMismatchException e)
										{
											l = (long) -1;
											s.nextLine();
										}
										if(l<0 || !numberToAccts.containsKey(l))
										{
											System.out.println("Please enter a valid account");
										}
										else
										{
											for(String str : numberToAccts.get(l).getOwners())
											{
												userToCustom.get(str).getAccts().remove(l);
											}
											numberToAccts.remove(l);
											System.out.println("Account deleted");
										}
										break;
									}
									default:
									{
										System.out.println("Invalid selection");
										break;
									}
								}
							}while(b!=7);
						}
					} 
					catch (LoginException e) {
						System.out.println(e.getMessage());
						state = 1;
					}
					break;
				}
				//registration screen
				case 3:
				{
					System.out.println("Please enter your intended username");
					user = s.nextLine();
					System.out.println("Please enter your intended password");
					pass = s.nextLine();
					if(b==2)
					{
						try {
							if(register(user,pass,(byte)(b-2),-1,userToCustom, employeeIDs))
							{
								System.out.println("Registration Successful!");
								System.out.println("Thank you for becoming a member of the bank!");
								state = 1;
								b = 0;
							}
							else
							{
								System.out.println("Registration Failed!");
								System.out.println("Username already used!");
							}
						} catch (UsernameUsedException e) {
							System.out.println(e.getMessage());
						}
					}
					else
					{
						System.out.println("Please enter your Employee ID");
						try 
						{
							l = s.nextLong();
							s.nextLine();
						}
						catch(InputMismatchException e)
						{
							l = (long) -1;
							s.nextLine();
						}
						if(l<0)
						{
							System.out.println("Please enter the a valid employee ID");
							state = 1;
							b = 0;
						}
						try 
						{
							if(register(user,pass,(byte)(b-2),l,userToCustom, employeeIDs))
							{
								System.out.println("Registration Successful!");
								System.out.println("Thank you for becoming an employee of the bank!");
							}
							else
							{
								System.out.println("Registration Failed!");
							}
						} 
						catch (UsernameUsedException e) 
						{
							System.out.println(e.getMessage());
						}
						state = 1;
						b = 0;
					}
					break;
				}
			}
			if(state == 0&& b == 5)
			{
				System.out.println("Thank you for using our service, Goodbye");
				break;
			}
		}
		System.out.println("Saving Data");
		saveAccounts(acctFile, numberToAccts);
		saveCustomers(customerFile, userToCustom);
		saveEmployeeIDs(idsFile,employeeIDs);
		System.out.println("Save Complete");
		s.close();
	}
}
