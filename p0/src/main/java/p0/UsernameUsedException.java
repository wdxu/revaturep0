package p0;

public class UsernameUsedException extends Exception {
	private static final long serialVersionUID = 1L;

	public UsernameUsedException(String message)
	{
		super(message);
	}
}
