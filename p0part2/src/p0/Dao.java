package p0;

import java.util.HashMap;

public interface Dao<K,T> {
    
    T get(K id);
    
    HashMap<K,T> getAll();
    
    void save(T t);
    
    void update(T t);
    
    void delete(K t);
    
    String listAll();
    
    String info(K t)throws AccountDoesNotExistException;
}