package p0;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
public class AccountDao implements Dao<Long,Account> {
	
	private HashMap<Long,Account> storage;
	private Connection conn;
	private long tempAcctNumber;
	public AccountDao(Connection con)
	{
		this.conn = con;
		this.storage = this.getAll();
	}
	public long acctNum()
	{
		return this.tempAcctNumber;
	}
	@Override
	public Account get(Long id) {
//		try 
//		{
//			int i = 0;
//			int j;
//			long acctNumber = 0;
//			double acctHoldings = 0;
//			byte status = 0;//0 = pending, 1 = accepted, 2 = rejected
//			boolean type = false;//joint = true, solo = false 
//			double depLimit = 0;//neg = no limit
//			double witLimit = 0;//neg = no limit
//			String[] owner;//name of owner(s)?
//			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";//simple
//			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
//			ps.setLong(1, id);
//			System.out.println(safeQuery);
//			ResultSet rs =ps.executeQuery();
//			String safeQuery2 = "SELECT * FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";//simple
//			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
//			ps2.setLong(1, id);
//			ResultSet rs2 =ps2.executeQuery();
//			String safeQuery3 = "SELECT COUNT(*) FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";//simple
//			PreparedStatement ps3 = conn.prepareStatement(safeQuery3);//precompiles the code and prevents sql injections
//			ps3.setLong(1, id);
//			ResultSet rs3 =ps3.executeQuery();
//			if(rs.next())//should only return 1 due to uniqueness
//			{
//				acctNumber = rs.getLong(1);
//				acctHoldings = rs.getDouble(2);
//				status = rs.getByte(3);
//				type = (rs.getInt(4)==1)?true:false;
//				depLimit = rs.getDouble(5);
//				witLimit = rs.getDouble(6);
//			}
//			if(rs3.next())
//			{
//				i = rs3.getInt(1);
//			}
//			owner = new String[i];
//			for(j = 0; j<i;j++)
//			{
//				rs2.next();
//				owner[j] = rs2.getString(1);
//			}
//			return new Account(acctNumber,acctHoldings,status,depLimit,witLimit,owner);
//		}
//		catch(SQLException e)
//		{
//			System.out.println("SQL EXCEPTION");
//			System.out.println(e.getMessage());
//		}
		if(this.storage.containsKey(id))
		{
			return this.storage.get(id);
		}
		return null;
	}
	
	@Override
	public HashMap<Long, Account> getAll() {
		HashMap<Long,Account> temp = new HashMap<Long,Account>();
		try 
		{
			int i = 0;
			int j;
			long acctNumber = 0;
			double acctHoldings = 0;
			byte status = 0;//0 = pending, 1 = accepted, 2 = rejected
			boolean type = false;//joint = true, solo = false 
			double depLimit = 0;//neg = no limit
			double witLimit = 0;//neg = no limit
			String[] owner;//name of owner(s)?
			String safeQuery = "SELECT * FROM ACCOUNTS";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ResultSet rs =ps.executeQuery();
			String safeQuery2 = "SELECT * FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";//simple
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ResultSet rs2;
			String safeQuery3 = "SELECT COUNT(*) FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";//simple
			PreparedStatement ps3 = conn.prepareStatement(safeQuery3);//precompiles the code and prevents sql injections
			ResultSet rs3;
			while(rs.next())
			{
				acctNumber = rs.getLong(1);
				acctHoldings = rs.getDouble(2);
				status = rs.getByte(3);
				type = (rs.getInt(4)==1)?true:false;
				depLimit = rs.getDouble(5);
				witLimit = rs.getDouble(6);
				ps2.setLong(1, acctNumber);
				rs2 =ps2.executeQuery();
				ps3.setLong(1, acctNumber);
				rs3 =ps3.executeQuery();
				if(rs3.next())
				{
					i = rs3.getInt(1);
				}
				owner = new String[i];
				for(j = 0; j<i;j++)
				{
					rs2.next();
					owner[j] = rs2.getString(1);
				}
				temp.put(acctNumber, new Account(acctNumber,acctHoldings,status,depLimit,witLimit,owner));
				this.tempAcctNumber = acctNumber;
			}
			System.out.println("Account loading successful");
			this.tempAcctNumber++;
			return temp;
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return storage;
	}

	@Override
	public void save(Account t) {
		if(this.storage.containsValue(t))
		{
			return;
		}
		this.storage.put(t.getAcctNumber(), t);
		try 
		{
			String safeQuery = "INSERT INTO ACCOUNTS\nVALUES (?,?,?,?,?,?)";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, t.getAcctNumber());
			ps.setDouble(2, t.getAcctHoldings());
			ps.setByte(3, t.getStatus());
			if(t.getType())
			{
				ps.setInt(4,1);
			}
			else
			{
				ps.setInt(4,0);
			}
			ps.setDouble(5, t.getDepLimit());
			ps.setDouble(6, t.getWitLimit());
			ps.executeUpdate();
			String safeQuery2 = "INSERT INTO USERACCOUNTLIST\nVALUES (?,?)";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			for(int i = 0; i<t.getOwners().length;i++)
			{
				ps2.setString(1, t.getOwners()[i]);
				ps2.setLong(2, t.getAcctNumber());
				ps2.executeUpdate();
			} 
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void update(Account t) {
		if(!this.storage.containsKey(t.getAcctNumber()))
		{
			return;
		}
		//this.storage.put(t.getAcctNumber(),t);
		try 
		{
			String safeQuery = "UPDATE ACCOUNTS\nSET ACCTHOLDINGS = ?, STATUS = ?, DEPLIMIT = ?, WITLIMIT = ? WHERE ACCTNUMBER = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setDouble(1, t.getAcctHoldings());
			ps.setByte(2, t.getStatus());
			ps.setDouble(3, t.getDepLimit());
			ps.setDouble(4, t.getWitLimit());
			ps.setLong(5, t.getAcctNumber());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}
	@Override
	public void delete(Long t) {
		if(!this.storage.containsKey(t))
		{
			return;
		}
		this.storage.remove(t);
		try {
			String safeQuery = "DELETE FROM ACCOUNTS WHERE ACCTNUMBER = ?";//simple
			String safeQuery2 = "DELETE FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, t);
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ps2.setLong(1, t);
			ps.executeUpdate();
			ps2.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}
	public boolean setStatus(Long t, Byte b, Byte type)
	{
		if(this.storage.containsKey(t))
		{
			if(this.storage.get(t).getStatus()!=0 && type!=2)
			{
				return false;
			}
			this.storage.get(t).setStatus(b);
			this.update(this.storage.get(t));
			return true;
		}
		return false;
	}

	public boolean setDepLimit(Long t, Double b, Byte type) throws AccountDoesNotExistException
	{
		if(this.storage.containsKey(t))
		{
			if(this.storage.get(t).getStatus()!=1 || type!=2)
			{
				return false;
			}
			this.storage.get(t).setDepLimit(b);
			this.update(this.storage.get(t));
			return true;
		}
		throw new AccountDoesNotExistException("Account does not exist"); 
	}

	public boolean setWitLimit(Long t, Double b, Byte type) throws AccountDoesNotExistException
	{
		if(this.storage.containsKey(t))
		{
			if(this.storage.get(t).getStatus()!=1 || type!=2)
			{
				return false;
			}
			this.storage.get(t).setWitLimit(b);
			this.update(this.storage.get(t));
			return true;
		}
		throw new AccountDoesNotExistException("Account does not exist"); 
	}
	public String listAllPending()
	{
		String s = "";
		try {
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE STATUS = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setByte(1,(byte) 0);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				s+=(rs.getString(1)+"\n");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			System.out.println("Attempting to print from data");
			int i = 0;
			for(; i<this.storage.keySet().toArray().length - 1; i++)
			{
				s+=this.storage.keySet().toArray()[i]+"\n";
			}
			if(this.storage.keySet().toArray().length>0)
			{
				s+=this.storage.keySet().toArray()[i];
			}
		}
		return s;
	}
	@Override
	public String listAll()
	{
		String s = "";
		try {
			String safeQuery = "SELECT * FROM ACCOUNTS";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				s+=(rs.getString(1)+"\n");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			System.out.println("Attempting to print from data");
			int i = 0;
			for(; i<this.storage.keySet().toArray().length - 1; i++)
			{
				s+=this.storage.keySet().toArray()[i]+"\n";
			}
			if(this.storage.keySet().toArray().length>0)
			{
				s+=this.storage.keySet().toArray()[i];
			}
		}
		return s;
	}
	public boolean transfer(String user, boolean owns, boolean admin, Long a, Long b, double amount) throws AccountDoesNotExistException
	{
		try 
		{
			if(a.equals(b))
			{
				throw new AccountDoesNotExistException("Destination account cannot be the same as source account!");
			}
			if((!owns)&&(!admin))
			{
				throw new AccountDoesNotExistException("You don't own this account!");
			}
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, a);
			ResultSet rs = ps.executeQuery();
			PreparedStatement ps2 = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps2.setLong(1, b);
			ResultSet rs2 = ps2.executeQuery();
			if(!rs.next())
			{
				throw new AccountDoesNotExistException("Source account does not exist!");
			}
			if(!rs2.next())
			{
				throw new AccountDoesNotExistException("Destination account does not exist!");
			}
			if(rs.getByte(3)==0)
			{
				throw new AccountDoesNotExistException("Source account pending");
			}
			if(rs2.getByte(3)==0)
			{
				throw new AccountDoesNotExistException("Destination account pending");
			}
			if(rs.getByte(3)==2)
			{
				throw new AccountDoesNotExistException("Source account rejected");
			}
			if(rs2.getByte(3)==2)
			{
				throw new AccountDoesNotExistException("Destination account rejected");
			}
			if(rs.getDouble(2)<amount)
			{
				return false;
			}
			else
			{
				this.storage.get(a).withdraw(amount);
				this.storage.get(b).deposit(amount);
				this.update(this.storage.get(a));
				this.update(this.storage.get(b));
				createTransferLog(user, (byte)2, a, b, amount);
				return true;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL Exception");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println("Transfer failed due to system being offline");
			return false;
		}
	}
	public boolean deposit(String user, boolean owns, boolean admin, Long a, double amount) throws AccountDoesNotExistException
	{
		try 
		{
			if((!owns)&&(!admin))
			{
				throw new AccountDoesNotExistException("You don't own this account!");
			}
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, a);
			ResultSet rs = ps.executeQuery();
			if(!rs.next())
			{
				throw new AccountDoesNotExistException("Account does not exist!");
			}
			if(rs.getByte(3)==0)
			{
				throw new AccountDoesNotExistException("Account pending");
			}
			if(rs.getByte(3)==2)
			{
				throw new AccountDoesNotExistException("Account rejected");
			}
			if(admin||(amount<rs.getDouble(5)||rs.getDouble(5)<0))
			{
				this.storage.get(a).deposit(amount);
				this.update(this.storage.get(a));
				createTransferLog(user, (byte)0, a, a, amount);
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL Exception");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println("Deposit failed due to system being offline");
		}
		return false;
	}
	public boolean withdraw(String user, boolean owns, boolean admin, Long a, double amount) throws AccountDoesNotExistException
	{
		try 
		{
			if((!owns)&&(!admin))
			{
				throw new AccountDoesNotExistException("You don't own this account!");
			}
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, a);
			ResultSet rs = ps.executeQuery();
			if(!rs.next())
			{
				throw new AccountDoesNotExistException("Account does not exist!");
			}
			if(rs.getByte(3)==0)
			{
				throw new AccountDoesNotExistException("Account pending");
			}
			if(rs.getByte(3)==2)
			{
				throw new AccountDoesNotExistException("Account rejected");
			}
			if((rs.getDouble(2)>amount)&&(amount<rs.getDouble(6)||(rs.getDouble(6)<0)||admin))
			{
				this.storage.get(a).withdraw(amount);
				this.update(this.storage.get(a));
				createTransferLog(user, (byte)1, a, a, amount);
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL Exception");
			System.out.println(e.getMessage());
			System.out.println("Withdrawal failed due to system being offline");
		}
		return false;
	}
	//todo: transfer
	//todo: deposit
	//todo: withdraw
	//create transfer log helper function, transfer logs cannot be deleted/updated due to the fact they're essential logs that
	//makes sure people don't mess up and things do what they're supposed to do... not even a local log is created
	public boolean createTransferLog(String user, byte type, long a, long b, double amount)
	{
		try 
		{
			String safeQuery = "INSERT INTO TRANSFERS\nVALUES (?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, user);
			ps.setByte(2, type);
			ps.setLong(3, a);
			ps.setLong(4, b);
			ps.setDouble(5, amount);
			ps.setString(6, java.time.LocalDate.now().toString());
			ps.setString(7, java.time.LocalTime.now().toString());
			ps.executeUpdate();
			return true;
		}
		catch(SQLException e)
		{
			System.out.println("SQL Exception");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println("Log creation failed");//if this happens, we have a serious problem, that means we didn't log something
		}
		return true;
	}
	public String viewTransferLogs(String date)
	{
		String s = "";
		try 
		{
			String safeQuery;
			PreparedStatement ps;
			if(date.equals(""))
			{
				safeQuery = "SELECT * FROM TRANSFERS";
				ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
				System.out.println("Printing all transfer logs");
			}
			else
			{
				safeQuery = "SELECT * FROM TRANSFERS WHERE DATE = ?";
				ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
				ps.setString(1, date);
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				s+= "User: " + rs.getString(1);
				switch(rs.getByte(2))
				{
					case 0:
						s+=" deposited into\n";
						s+="Account A:" + rs.getString(3) + "\n";
						break;
					case 1:
						s+=" withdrew from\n";
						s+="Account A:" + rs.getString(3) + "\n";
						break;
					case 2:
						s+=" initiated the transfer between\n";
						s+="Account A: " + rs.getString(3) + "and\n";
						s+="Account B: " + rs.getString(4) + "\n";
						break;
				}
				s+= "For " + String.format("%.2f", rs.getDouble(5)) + "\n";
				s+= "On " + rs.getString(6)+ "At, "+rs.getString(7)+"\n";
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL Exception");
			System.out.println("Log search failed");
		}
		return s;
	}
	@Override
	public String info(Long t) throws AccountDoesNotExistException
	{
		String s = "";
		try {
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, t);
			String safeQuery2 = "SELECT * FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ps2.setLong(1, t);
			String safeQuery3 = "SELECT COUNT(*) FROM USERACCOUNTLIST WHERE ACCTNUMBER = ?";
			PreparedStatement ps3 = conn.prepareStatement(safeQuery3);//precompiles the code and prevents sql injections
			ps3.setLong(1, t);
			ResultSet rs = ps.executeQuery();
			ResultSet rs2 = ps2.executeQuery();
			ResultSet rs3 = ps3.executeQuery();
			rs3.next();
			if(rs.next())
			{
				s+="Account Number: " + rs.getLong(1)+ "\n";
				s+="Account Holdings:" + String.format("%.2f",rs.getDouble(2)) + "\n";
				s+="Status: ";
				switch(rs.getByte(3))
				{
					case 0:
						s+="Pending\n";
						break;
					case 1:
						s+="Accepted\n";
						break;
					case 2:
						s+="Rejected\n";
						break;
				}
				s+="Type: ";
				if(rs.getByte(4)==1)
				{
					s+= "Joint\n";
				}
				else
				{
					s+= "Personal\n";
				}
				s+="Deposit Limit: " + rs.getDouble(5) + "\n";
				s+="Withdrawal Limit: " + rs.getDouble(6) + "\n";
				s+="This account has " + rs3.getInt(1) + " owner(s)\n";
				while(rs2.next())
				{
					s+=(rs2.getString(1)+"\n");
				}
			}
			return s;
		}
		catch(SQLException e)
		{
			int i = 0;
			if(this.storage.containsKey(t))
			{
				s+="Account Number: " + this.storage.get(t).getAcctNumber()+ "\n";
				s+="Account Holdings:" + this.storage.get(t).getAcctHoldings() + "\n";
				s+="Status: ";
				switch(this.storage.get(t).getStatus())
				{
					case 0:
						s+="Pending\n";
						break;
					case 1:
						s+="Accepted\n";
						break;
					case 2:
						s+="Rejected\n";
						break;
					
				}
				s+="Type: ";
				if(this.storage.get(t).getType())
				{
					s+= "Joint\n";
				}
				else
				{
					s+= "Personal\n";
				}
				s+="Deposit Limit: " + this.storage.get(t).getDepLimit() + "\n";
				s+="Withdrawal Limit: " + this.storage.get(t).getWitLimit() + "\n";
				s+="This account has " + this.storage.get(t).getOwners().length + " owner(s)\n";
				for(; i<this.storage.get(t).getOwners().length;i++)
				{
					s+=this.storage.get(t).getOwners()[i]+"\n";
				}
			}
			else
			{
				throw new AccountDoesNotExistException("Bank account not found");
			}
			return s;
		}
	}
	boolean requestAccount(Long acctNum, String[] own)//, CustomerDao cd)
	{
		if(this.storage.containsKey(acctNum))
		{
			return false;
		}
		Account act = new Account(acctNum,own);
		this.save(act);//update database
//		for(String i : own)//update local
//		{
//			cd.storage.get(i).addAcct(acctNum);
//		}
		return true;
	}
}
