package p0;

import java.io.Serializable;

//todo: joint transfers
public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5094570976638583517L;
	private long acctNumber;
	private double acctHoldings;
	private byte status;//0 = pending, 1 = accepted, 2 = rejected
	private boolean type;//joint = true, solo = false 
	private double depLimit;//neg = no limit
	private double witLimit;//neg = no limit
	private String[] owner;//name of owner(s)?
	public Account(long acctNum, String[] own)
	{
		this.acctNumber = acctNum;
		this.acctHoldings = 0;
		this.status = 0;
		if(own.length>1)
		{
			this.type = true;
		}
		else
		{
			this.type = false;
		}
		this.depLimit = -1;
		this.witLimit = -1;
		this.owner = own;
	}
	public Account(long acctNum,double acctHold, byte stat,double depLim,double witLim,String[] own)
	{
		this.acctNumber = acctNum;
		this.acctHoldings = acctHold;
		this.status = stat;
		if(own.length>1)
		{
			this.type = true;
		}
		else
		{
			this.type = false;
		}
		this.depLimit = depLim;
		this.witLimit = witLim;
		this.owner = own;
	}
//	public Account(long acctNum, String[] own,double dLim,double wLim)
//	{
//		this.acctNumber = acctNum;
//		this.acctHoldings = 0;
//		this.status = 0;
//		this.type = false;
//		this.depLimit = dLim;
//		this.witLimit = wLim;
//		this.owner = own;
//	}
	//standard getters and setters along with deposit and withdrawing methods
	public long getAcctNumber() {
		return acctNumber;
	}
	public double getAcctHoldings() {
		return acctHoldings;
	}
	public double withdraw(double amount) {
		if(amount<=this.acctHoldings)
		{
			this.acctHoldings -= amount;	
		}
		return this.acctHoldings;
	}
	public double deposit(double amount) {
		this.acctHoldings += amount;
		return this.acctHoldings;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public boolean getType() {
		return type;
	}
	public double getDepLimit() {
		return depLimit;
	}
	public void setDepLimit(double depLimit) {
		this.depLimit = depLimit;
	}
	public double getWitLimit() {
		return witLimit;
	}
	public void setWitLimit(double witLimit) {
		this.witLimit = witLimit;
	}
	public String[] getOwners()
	{
		return this.owner;
	}
	public String toString()
	{
		int i;
		String temp = "";
		for(i = 0; i<this.owner.length-1;i++)
		{
			temp+= owner[i]+"\n";
		}
		if(this.owner.length!=0)//bug if ever true
		{
			temp+=owner[i];
		}
		return ""+this.acctNumber+"\n"+this.acctHoldings+"\n"+this.status+"\n"+this.depLimit+"\n"+this.witLimit+"\n"+this.type+"\n"+this.owner.length+"\n"+temp;
	}
}
