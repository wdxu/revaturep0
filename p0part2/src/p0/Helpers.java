package p0;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//used for keeping track of stuff to implement
public class Helpers {

	public boolean containsUser(String name,Connection conn)//finds username in the users table 
	{
		try {
			String safeQuery = "SELECT * FROM USERS WHERE USERNAME = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, name);
			System.out.println(safeQuery);
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return true;
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	public boolean containsAccount(Long account,Connection conn)//finds username in the users table 
	{
		try {
			String safeQuery = "SELECT * FROM ACCOUNTS WHERE ACCTNUMBER = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, account.toString());
			System.out.println(safeQuery);
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return true;
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	public boolean containsUnusedID(Long id,Connection conn)//finds username in the users table 
	{
		try {
			String safeQuery = "SELECT * FROM IDS WHERE IDS = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			System.out.println(safeQuery);
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return (rs.getInt("USED")==0);
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	public boolean createUserAccount(String username, String pswd, Byte t, Long iden, Connection conn)//finds username in the users table 
	{
		try {
			String safeUpdate = "INSERT INTO USERS\nVALUES (?,?,?,?,?)";//simple
			PreparedStatement ps = conn.prepareStatement(safeUpdate);//precompiles the code and prevents sql injections
			ps.setString(1, username);
			ps.setString(2, pswd);
			ps.setString(3, iden.toString());
			ps.setString(4, t.toString());
			ps.setString(5, "0");
			System.out.println(safeUpdate);
			ps.executeUpdate();
			return true;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	public boolean createTransferLog(String username, Byte t,Long accta, Long acctb, Double amount,Connection conn)//finds username in the users table 
	{
		try {
			String safeUpdate = "INSERT INTO USERS\nVALUES (?,?,?,?,?,?)";//simple
			PreparedStatement ps = conn.prepareStatement(safeUpdate);//precompiles the code and prevents sql injections
			ps.setString(1, username);
			ps.setString(2, t.toString());
			ps.setString(3, accta.toString());
			if(t==2)
			{
				ps.setString(4, acctb.toString());
			}
			ps.setString(5, amount.toString());
			ps.setString(6, java.time.LocalDate.now().toString());
			System.out.println(safeUpdate);
			ps.executeUpdate();
			return true;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	//used for creation of a new id
	public boolean containsID(Long id,Connection conn)//finds username in the users table 
	{
		try {
			String safeQuery = "SELECT * FROM IDS WHERE IDS = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			System.out.println(safeQuery);
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return true;
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	//used for creation of a new id
	public boolean createBankAcct(Long id,String[] owners,Double depLimit,Double witLimit,Connection conn)//finds username in the users table 
	{
		try {
			String safeQuery = "INSERT INTO ACCOUNTS\nVALUES (?,?,?,?,?,?)";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			ps.setString(2, "0.0");
			ps.setString(3, "0");
			if(owners.length>1)
			{
				ps.setString(4, "1");	
			}
			else
			{
				ps.setString(4, "0");
			}
			ps.setString(5, depLimit.toString());
			ps.setString(6, witLimit.toString());
			System.out.println(safeQuery);
			ps.executeUpdate();
			for(int i = 0; i<owners.length; i++)
			{
				safeQuery = "INSERT INTO USERACCOUNTLIST\nVALUES(?,?)";
				ps = conn.prepareStatement(safeQuery);
				ps.setString(1, owners[i]);
				ps.setString(2, id.toString());
				ps.executeUpdate();
			}
			return true;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
}
