package p0;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AccountTest {

	String[] ownerList = {"pawn","king","queen","rook","bishop","knight"};
	String[] ownerList2 = {"singles"};
	Account testing;
	Account testing2;
	Account testing3;
	Account testing4;
	@Before
	public void initialization()
	{
		testing = new Account(1337, ownerList);
		testing2 = new Account(420,0,(byte)1,9999,9999, ownerList);
		testing3 = new Account(6969, ownerList2);
		testing4 = new Account(911,0,(byte)0,6000,8000, ownerList2);
	}
	
	@Test
	public void initializationTest()
	{
		//initialization and getter tests
		assertEquals(testing.getAcctNumber(),1337);
		assertTrue(testing.getAcctHoldings() == 0);
		assertTrue(testing.getStatus() == 0);
		assertTrue(testing.getDepLimit() == -1);
		assertTrue(testing.getWitLimit() == -1);
		assertTrue(testing.getType());
		assertTrue(testing.getOwners().length == ownerList.length);
		for(int i = 0;i<testing.getOwners().length;i++)
		{
			assertEquals(ownerList[i],testing.getOwners()[i]);
		}
		assertEquals(testing2.getAcctNumber(),420);
		assertTrue(testing2.getAcctHoldings() == 0);
		assertTrue(testing2.getStatus() == 1);
		assertTrue(testing2.getDepLimit() == 9999);
		assertTrue(testing2.getWitLimit() == 9999);
		assertTrue(testing2.getType());
		assertTrue(testing2.getOwners().length == ownerList.length);
		for(int i = 0;i<testing2.getOwners().length;i++)
		{
			assertEquals(ownerList[i],testing2.getOwners()[i]);
		}
		assertEquals(testing3.getAcctNumber(),6969);
		assertTrue(testing3.getAcctHoldings() == 0);
		assertTrue(testing3.getStatus() == 0);
		assertTrue(testing3.getDepLimit() == -1);
		assertTrue(testing3.getWitLimit() == -1);
		assertTrue(!testing3.getType());
		assertTrue(testing3.getOwners().length == ownerList2.length);
		for(int i = 0;i<testing3.getOwners().length;i++)
		{
			assertEquals(ownerList2[i],testing3.getOwners()[i]);
		}
		assertEquals(testing4.getAcctNumber(),911);
		assertTrue(testing4.getAcctHoldings() == 0);
		assertTrue(testing4.getStatus() == 0);
		assertTrue(testing4.getDepLimit() == 6000);
		assertTrue(testing4.getWitLimit() == 8000);
		assertTrue(!testing4.getType());
		assertTrue(testing4.getOwners().length == ownerList2.length);
		for(int i = 0;i<testing4.getOwners().length;i++)
		{
			assertEquals(ownerList2[i],testing4.getOwners()[i]);
		}
	}
	@Test
	public void setStatusTest()
	{
		testing.setStatus((byte) 1);
		assertTrue(testing.getStatus() == 1);
		testing2.setStatus((byte) 2);
		assertTrue(testing2.getStatus() == 2);
		testing3.setStatus((byte) 0);
		assertTrue(testing3.getStatus() == 0);
		testing4.setStatus((byte) 1);
		assertTrue(testing4.getStatus() == 1);
	}
	@Test
	public void setDepLimitTest()
	{
		testing.setStatus((byte) 1);
		assertTrue(testing.getStatus() == 1);
		testing2.setStatus((byte) 1);
		assertTrue(testing2.getStatus() == 1);
		testing3.setStatus((byte) 1);
		assertTrue(testing3.getStatus() == 1);
		testing4.setStatus((byte) 1);
		assertTrue(testing4.getStatus() == 1);
		testing.setDepLimit(9999);
		assertTrue(testing.getDepLimit() == 9999);
		testing2.setDepLimit(420);
		assertTrue(testing2.getDepLimit() == 420);
		testing3.setDepLimit(911);
		assertTrue(testing3.getDepLimit() == 911);
		testing4.setDepLimit(6969);
		assertTrue(testing4.getDepLimit() == 6969);
	}
	@Test
	public void setWitLimitTest()
	{
		testing.setWitLimit(9999);
		assertTrue(testing.getWitLimit() == 9999);
		testing2.setWitLimit(420);
		assertTrue(testing2.getWitLimit() == 420);
		testing3.setWitLimit(911);
		assertTrue(testing3.getWitLimit() == 911);
		testing4.setWitLimit(6969);
		assertTrue(testing4.getWitLimit() == 6969);
	}
	
	@Test
	public void depositTest()
	{
		testing.deposit(2000);
		assertTrue(testing.getAcctHoldings() == 2000);
		testing.deposit(2000);
		assertTrue(testing.getAcctHoldings() == 4000);
		testing2.deposit(2000);
		assertTrue(testing2.getAcctHoldings() == 2000);
		testing2.deposit(2000);
		assertTrue(testing2.getAcctHoldings() == 4000);
		testing3.deposit(2000);
		assertTrue(testing3.getAcctHoldings() == 2000);
		testing3.deposit(2000);
		assertTrue(testing3.getAcctHoldings() == 4000);
		testing4.deposit(2000);
		assertTrue(testing4.getAcctHoldings() == 2000);
		testing4.deposit(2000);
		assertTrue(testing4.getAcctHoldings() == 4000);
	}
	
	@Test
	public void withdrawTest() {
		//deposit and withdraw
		testing.deposit(2000);
		assertTrue(testing.getAcctHoldings() == 2000);
		testing.withdraw(500);
		assertTrue(testing.getAcctHoldings() == 1500);
		testing2.deposit(3000);
		assertTrue(testing2.getAcctHoldings() == 3000);
		testing2.withdraw(400);
		assertTrue(testing2.getAcctHoldings() == 2600);
		testing3.deposit(900);
		assertTrue(testing3.getAcctHoldings() == 900);
		testing3.withdraw(500);
		assertTrue(testing3.getAcctHoldings() == 400);
		testing4.deposit(200);
		assertTrue(testing4.getAcctHoldings() == 200);
		testing4.withdraw(100);
		assertTrue(testing4.getAcctHoldings() == 100);
	}
	@Test
	public void toStringTest() {
		assertTrue(testing.toString().equals("1337\n0.0\n0\n-1.0\n-1.0\ntrue\n6\npawn\nking\nqueen\nrook\nbishop\nknight"));
		assertTrue(testing2.toString().equals("420\n0.0\n1\n9999.0\n9999.0\ntrue\n6\npawn\nking\nqueen\nrook\nbishop\nknight"));	
		assertTrue(testing3.toString().equals("6969\n0.0\n0\n-1.0\n-1.0\nfalse\n1\nsingles"));
		assertTrue(testing4.toString().equals("911\n0.0\n0\n6000.0\n8000.0\nfalse\n1\nsingles"));	
	}
}
