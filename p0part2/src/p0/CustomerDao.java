package p0;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;

public class CustomerDao implements Dao<String, Customer> {

	HashMap<String, Customer> storage;
	Connection conn;
	public CustomerDao(Connection con)
	{
		this.conn = con;
		this.storage = this.getAll();
	}
	@Override
	public Customer get(String id) {
		if(this.storage.containsKey(id))
		{
			return this.storage.get(id);
		}
		return null;
	}

	/**
	 * @param username self explanatory
	 * @param password self explanatory
	 * @param userToCustom username to customer hashmap
	 * @return if login successful
	 * @throws LoginException 
	 */
	public boolean login(String username, String password) throws LoginException
	{
		if(this.get(username)==null)
		{
			throw new LoginException("Username not found");
		}
		if(!(this.get(username).getPassword().equals(password)))
		{
			throw new LoginException("Incorrect Password");
		}
		return true;
	}

	/**
	 * @param username  self explanatory
	 * @param password  self explanatory
	 * @param type 0 = customer, 1 = employee, 2 = admin
	 * @param id employeeID, if type = 0, id should be -1
	 * @return returns whether the registration is successful
	 * @throws UsernameUsedException 
	 */
	public boolean register(String username, String password, byte type, long id) throws UsernameUsedException
	{
		//boolean used to see if id has been used
		if(this.get(username)!=null)
		{
			throw new UsernameUsedException("This username has already been used");
		}
		if(type!=0)
		{
			//if the employee ID doesn't exist in the database
			if((!containsUnusedID(id)))
			{
				throw new UsernameUsedException("This ID has already been used");
			}
			usedUnusedID(id);
		}
		Customer user = new Customer(username, password, type, id);
		this.save(user);
		return true;
	}
	//used to get everything
	@Override
	public HashMap<String, Customer> getAll() 
	{
		HashMap<String, Customer> temp = new HashMap<String, Customer>();
		try {
			String safeQuery = "SELECT * FROM USERS";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			String safeQuery2 = "SELECT * FROM USERACCOUNTLIST WHERE USERNAME = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ResultSet rs = ps.executeQuery();
			ResultSet rs2;
			String user;
			String pass;
			long id;
			byte type;
			LinkedList<Long> accts;
			while(rs.next())
			{
				accts = new LinkedList<Long>();
				user = rs.getString(1);
				pass = rs.getString(2);
				type = rs.getByte(3);
				id = rs.getLong(4);
				ps2.setString(1, user);
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					accts.add(rs2.getLong(2));
				}
				temp.put(user, new Customer(user,pass,type,id,accts));
			}
			System.out.println("Customer loading successful");
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return temp;
	}

	//used for customer creation
	@Override
	public void save(Customer t) {
		if(this.storage.containsValue(t))
		{
			return;
		}
		this.storage.put(t.getUser(),t);
		try 
		{
			String safeQuery = "INSERT INTO USERS\nVALUES (?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, t.getUser());
			ps.setString(2, t.getPassword());
			ps.setByte(3, t.getType());
			ps.setLong(4, t.getID());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public void update(Customer t) {
		if(!this.storage.containsKey(t.getUser()))
		{
			return;//it don't exist, don't update it
		}
		try 
		{
			String safeQuery = "UPDATE USERS\nSET PASSWORD = ?, TYP = ?, ID = ? WHERE USERNAME = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, t.getPassword());
			ps.setByte(2, t.getType());
			ps.setLong(3, t.getID());
			ps.setString(4, t.getUser());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}

	//probably never used, due to the fact that people will to the most have their accts frozen, not deleted
	@Override
	public void delete(String t) {
		if(!this.storage.containsKey(t))
		{
			return;
		}
		this.storage.remove(t);
		try {
			String safeQuery = "DELETE FROM USERS WHERE USERNAME = ?";//simple
			String safeQuery2 = "DELETE FROM USERACCOUNTLIST WHERE USERNAME = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, t);
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ps2.setString(1, t);
			ps.executeUpdate();
			ps2.executeUpdate();
			//think about acct deletion/ joint acct name removal
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
	}
	public void addAcct(String[] owners, Long acctNumber) 
	{
		for(String i: owners)
		{
			this.storage.get(i).addAcct(acctNumber);
			//this.update(this.storage.get(i));//nothing to update, all done in AccountDao
		}
	}

	public void removeAcct(String[] owners, Long acctNumber) 
	{
		for(String i: owners)
		{
			this.storage.get(i).removeAcct(acctNumber);
			//this.update(this.storage.get(i));//nothing to update, all done in AccountDao
		}
	}
	public boolean changePswd(String owner, String pswd) 
	{
		if(pswd.equals(""))
		{
			return false;
		}
		this.storage.get(owner).changePassword(pswd);
		this.update(this.storage.get(owner));
		return true;
	}
	@Override
	public String listAll() 
	{
		String s = "";
		try 
		{
			String safeQuery = "SELECT * FROM USERS";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				s+=(rs.getString(1)+"\n");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			System.out.println("Attempting to print from data");
			int i = 0;
			for(; i<this.storage.keySet().toArray().length - 1; i++)
			{
				s+=this.storage.keySet().toArray()[i]+"\n";
			}
			if(this.storage.keySet().toArray().length>0)
			{
				s+=this.storage.keySet().toArray()[i];
			}
		}
		return s;
	}
	@Override
	public String info(String t) throws AccountDoesNotExistException 
	{
		String s = "";
		try 
		{
			String safeQuery = "SELECT * FROM USERS WHERE USERNAME = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, t);
			String safeQuery2 = "SELECT * FROM USERACCOUNTLIST WHERE USERNAME = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ps2.setString(1, t);
			String safeQuery3 = "SELECT COUNT(*) FROM USERACCOUNTLIST WHERE USERNAME = ?";
			PreparedStatement ps3 = conn.prepareStatement(safeQuery3);//precompiles the code and prevents sql injections
			ps3.setString(1, t);
			ResultSet rs = ps.executeQuery();
			ResultSet rs2 = ps2.executeQuery();
			ResultSet rs3 = ps3.executeQuery();
			rs3.next();
			if(rs.next())
			{
				s+="Username: " + rs.getString(1) + "\n";
				s+="Password: " + rs.getString(2) + "\n";
				s+="Type: ";
				switch(rs.getByte(3))
				{
					case 0:
						s+="customer\n";
						break;
					case 1:
						s+="employee\n";
						break;
					case 2:
						s+="admin\n";
						break;
					
				}
				s+="ID: " + rs.getLong(4) + "\n";
				s+="This user has " + rs3.getInt(1) + " accounts\n";
				while(rs2.next())
				{
					s+= rs2.getLong(2)+"\n";
				}
			}
			else
			{
				throw new AccountDoesNotExistException("Account not found");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			System.out.println("Attempting to print from data");
			int i = 0;
			if(this.storage.containsKey(t))
			{
				s+="Username: " + this.storage.get(t).getUser() + "\n";
				s+="Password: " + this.storage.get(t).getPassword() + "\n";
				s+="Type: ";
				switch(this.storage.get(t).getType())
				{
					case 0:
						s+="customer\n";
						break;
					case 1:
						s+="employee\n";
						break;
					case 2:
						s+="admin\n";
						break;
					
				}
				s+="ID: " + this.storage.get(t).getID() + "\n";
				s+="This user has " + this.storage.get(t).getAccts().size() + " accounts\n";
				for(; i<this.storage.get(t).getAccts().size();i++)
				{
					s+=this.storage.get(t).getAccts().get(i).toString()+"\n";
				}
			}
			else
			{
				throw new AccountDoesNotExistException("Account not found");
			}
		}
		return s;
	}
	
	//technically a mostly static helper method, used in resgistration for employees
	public boolean containsUnusedID(Long id) 
	{
		try {
			String safeQuery = "SELECT * FROM IDS WHERE IDS = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return (rs.getInt("USED")==0);
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}	
	//helper methods, used, but unused in this class,
	//used for registration of a new acct
	public boolean createUnusedID(Long id)//finds username in the users table 
	{
		if(containsID(id))
		{
			return false;
		}
		try {
			String safeUpdate = "INSERT INTO IDS\nVALUES (?,0)";//simple
			PreparedStatement ps = conn.prepareStatement(safeUpdate);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			ps.executeUpdate();
//			if(rs.next())
//			{
//				return (rs.getInt("USED")==0);
//			}
			return true;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}	
	//
	public boolean usedUnusedID(Long id)//finds username in the users table 
	{
		if(!containsUnusedID(id))
		{
			System.out.println("used unused failed");
			return false;
		}
		try {
			String safeUpdate = "UPDATE IDS\nSET USED = ? WHERE IDS = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeUpdate);//precompiles the code and prevents sql injections
			ps.setInt(1, 1);
			ps.setLong(2, id);
			ps.executeUpdate();
//			if(rs.next())
//			{
//				return (rs.getInt("USED")==0);
//			}
			return true;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}	
	//used for creation of a new id
	public boolean containsID(Long id)//finds username in the users table 
	{
		try {
			String safeQuery = "SELECT * FROM IDS WHERE IDS = ?";//simple
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, id.toString());
			ResultSet rs =ps.executeQuery();
			if(rs.next())
			{
				return true;
			}
			return false;
		}
		catch (SQLException e)
		{
			System.out.println("SQLException");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
}
