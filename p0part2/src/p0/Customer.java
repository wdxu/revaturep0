package p0;

import java.io.Serializable;
import java.util.LinkedList;

public class Customer implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7018043958059972111L;
	private String user;
	private String password;
	private long id;
	//private String name;
	//private String email;
	//private double depLimit;
	//private double witLimit;
	private byte type;
	private LinkedList<Long> accts;
	//private LinkedList<Account> accountList;
	public Customer(String username, String pswd, byte t, long iden)
	{
		this.user = username;
		this.password = pswd;
		//this.depLimit = -1;//infinite
		//this.witLimit = -1;//infinite
		this.type = t;
		id = iden;
		this.accts = new LinkedList<Long>();
		//"pointless info" to be added
		//this.accountList = new LinkedList<Account>();
	}
	public Customer(String username, String pswd,/* double depLim, double witLim,*/ byte t, long iden, LinkedList<Long> acct)
	{
		this.user = username;
		this.password = pswd;
		//this.depLimit = depLim;//infinite
		//this.witLimit = witLim;//infinite
		this.id = iden;
		this.type = t;
		this.accts = acct;
		//this.accountList = new LinkedList<Account>();
	}
	public boolean changePassword(String nPswd)
	{
		this.password = nPswd;
		return true;
	}
	public String getUser()
	{
		return this.user;
	}
	public String getPassword()
	{
		return this.password;
	}
//	public double getDepLimit()
//	{
//		return this.depLimit;
//	}
//	public boolean setDepLimit(double limit)
//	{
//		this.depLimit = limit;
//		return true;
//	}
//	public double getWitLimit()
//	{
//		return witLimit;
//	}
//	public boolean setWitLimit(double limit)
//	{
//		this.witLimit = limit;
//		return true;
//	}
	public long getID()
	{
		return this.id;
	}
	public byte getType() {
		return type;
	}
//	public void setType(byte type, long id) {
//		this.type = type;
//		this.id = id;
//	}
	public LinkedList<Long> getAccts() {
		return accts;
	}
	public boolean removeAcct(Long l) {
		return this.accts.remove(l);
	}
	public boolean addAcct(Long l) {
		if(this.accts.contains(l))
		{
			return false;
		}
		this.accts.add(l);
		return true;
	}
	public String toString()
	{
		int i = 0;
		String s = "";
		for(; i<this.accts.size()-1;i++)
		{
			s+=this.accts.get(i).toString()+"\n";
		}
		if(this.accts.size()!=0)
		{
			s+=this.accts.get(i).toString();
		}
		return "" + this.user + "\n" + this.password + "\n" /* + this.depLimit + "\n" + this.witLimit+ "\n" */ + this.type + "\n" + this.id + "\n" + this.accts.size() + "\n" + s;
	}
	/**public boolean requestAccount(long acctNum)
	{
		Account n = new Account(acctNum);
		return this.accountList.add(n);
	}
	public boolean deleteAccount(long acctNum)
	{
		for(int i = 0; i<this.accountList.size();i++)
		{
			if(accountList.get(i).getAcctNumber()==acctNum)
			{
				accountList.remove(i);
				return true;
			}
		}
		return false;
	}
	//from acc1, to acc2
	public boolean transfer(long acc1, Account acc2, double amount)
	{
		int i = 0;
		for(; i<this.accountList.size(); i++)
		{
			if(this.accountList.get(i).getAcctNumber() == acc1)
			{
				if(this.accountList.get(i).getWitLimit()<amount)
				{
					return false;
				}
				break;
			}
		}
		/**if(acc2.getDepLimit() < amount)
		{
			return false;
		}* /
		return true;
	}*/
}

