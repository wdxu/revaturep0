async function login()
{
    let u = document.getElementById("username").value;
    let p = document.getElementById("password").value;
    //let manager = "managerLink";
    //let employee = "employeeLink";
    let loginCombo = {
        username : u,
        password : p,
        isManager : (10>9) //don't care for now
    };
    console.log(loginCombo.username);
    console.log(loginCombo.password);
	console.log(loginCombo);
    let response = await fetch("/p1/LoginServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(loginCombo)});
    let result = await response.text();
    console.log(result);
	if(result =="YM")
	{
		window.location.replace("manager.html");
	}
	if(result =="YE")
	{
		window.location.replace("employee.html");
	}
	if(result == "N")
	{
		alert("Login failed");
	}
    return result;
}

//function hello()
//{
//	alert("Onload test");
//}

//window.onload = hello();
