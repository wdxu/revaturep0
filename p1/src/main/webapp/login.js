async function login()
{
	document.getElementById('error').innerHTML = "";
    let u = document.getElementById("username").value;
    let p = document.getElementById("password").value;
    //let manager = "managerLink";
    //let employee = "employeeLink";
    let loginCombo = {
        username : u,
        password : p,
        isManager : (10>9) //don't care for now
    };
    console.log(loginCombo.username);
    console.log(loginCombo.password);
	console.log(loginCombo);
    let response = await fetch("/p1/LoginServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(loginCombo)});
    let result = await response.text();
    console.log(result);
	if(result =="YM")
	{
		window.location.replace("http://localhost:8080/p1/manager.html");
	}
	if(result =="YE")
	{
		window.location.replace("http://localhost:8080/p1/employee.html");
	}
	if(result == "N")
	{
		 document.getElementById('error').innerHTML = "<p style=\"color:red\"> Login failed, please check your username and password! <p>"
	}
    return result;
}

function initListeners()
{
	document.addEventListener('keypress', function (e) 
	{
		if(e == null)
			return;
		console.log(e.key);
	    if (e.key === 'Enter') 
		{
			//checks whether the pressed key is "Enter"
	        login();
	    }
	}, false);
}

//function hello()
//{
//	alert("Onload test");
//}

window.onload = initListeners();
