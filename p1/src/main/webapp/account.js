async function getUserInfo()
{
    let response = await fetch("/p1/GetUserInfoServlet", 
								{method:"POST"});
	var user = await response.json();
	document.getElementById("password").value = user.password;
	document.getElementById("firstname").value = user.firstname;
	document.getElementById("lastname").value = user.lastname;
	document.getElementById("username").innerHTML = "<p><b>Username:</b> " + user.username + "</p>";
	document.getElementById("email").innerHTML = "<p><b>Email:</b> " + user.email + "</p>";
	document.getElementById("id").innerHTML = "<p><b>ID:</b> " + user.id + "</p>";
	if(user.isManager == true)
	{
		document.getElementById("role").innerHTML = "<p><b>Role:</b> Manager</p>";
	}
	if(user.isManager == false)
	{
		document.getElementById("role").innerHTML = "<p><b>Role:</b> Employee</p>";
	}
}

async function updateUserInfo()
{
	p = document.getElementById("password").value;
	f = document.getElementById("firstname").value;
	l = document.getElementById("lastname").value;
	    let user = {
        password : p,
		firstname : f,
		lastname : l
    };
    let response = await fetch("/p1/UpdateUserInfoServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(user)});
    let result = await response.text();
	if(result == "succ")
	{
		alert("account info change successful");
	}
	if(result == "fail")
	{
		alert("account info change failed");
	}
	getUserInfo();
}

async function sessionValidation()
{
    let response = await fetch("/p1/SessionValidationServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result == "fail")
	{
		window.location.replace("login.html");
	}
	if(result == "succE")
	{
		if(window.location.href != "http://localhost:8080/p1/employee.html")
		{
			window.location.replace("http://localhost:8080/p1/employee.html");
		}
	}
	if(result == "succM")
	{
		if(window.location.href != "http://localhost:8080/p1/manager.html")
		{
			window.location.replace("http://localhost:8080/p1/manager.html");
		}
	}
}
async function logout()
{
    let response = await fetch("/p1/LogoutServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result =="LoggedOut")
	{
		window.location.replace("login.html");
	}
    else
    {
        console.log("log off failed???");
    }
    return result;
}
window.addEventListener("load",function() {
    sessionValidation();
	getUserInfo();
},false);

//window.addEventListener("load",function(){
//},false);