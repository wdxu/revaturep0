package p1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RequestDao {
	private Connection conn;
	public RequestDao(Connection c)
	{
		conn = c;
	}
	//used by managers
	public Request[] getRequests()
	{
		System.out.println("Sending All");
		Request r[] = null;
		int i = 0;
		try 
		{
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ResultSet rs = ps.executeQuery();
			String safeQuery2 = "SELECT * FROM REQUESTS";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ResultSet rs2;
			if(rs.next())
			{
				r = new Request[rs.getInt(1)];
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					r[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					System.out.println("ret insert count: "+i);
					i++;
				}
			}
			else
			{
				return r;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return r;
	}
	//used by managers
	public Request[] getRequestsByStatus(byte s)
	{
		Request r[] = null;
		int i = 0;
		try 
		{
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS WHERE STATUS = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setByte(1, s);
			ResultSet rs = ps.executeQuery();
			String safeQuery2 = "SELECT * FROM REQUESTS WHERE STATUS = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ps2.setByte(1, s);
			ResultSet rs2;
			if(rs.next())
			{
				r = new Request[rs.getInt(1)];
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					r[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					i++;
				}
			}
			else
			{
				return r;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return r;
	}

	public Request[] getRequestByID(long id) 
	{
		Request r[] = null;
		int i = 0;
		try 
		{
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS WHERE REQUESTID = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			String safeQuery2 = "SELECT * FROM REQUESTS WHERE REQUESTID = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ps2.setLong(1, id);
			ResultSet rs2;
			if(rs.next())
			{
				r = new Request[rs.getInt(1)];
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					r[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					i++;
				}
			}
			else
			{
				return r;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return r;
	}
	//used by managers
	public Request[] getRequestsByDate(String date)
	{
		Request r[] = null;
		int i = 0;
		try 
		{
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS WHERE \"DATE\" = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, date);
			ResultSet rs = ps.executeQuery();
			String safeQuery2 = "SELECT * FROM REQUESTS WHERE \"DATE\" = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ps2.setString(1, date);
			ResultSet rs2;
			if(rs.next())
			{
				r = new Request[rs.getInt(1)];
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					r[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					i++;
				}
			}
			else
			{
				return r;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return r;
	}
	//used by everyone
	public Request[] getRequestsByUsername(String username)
	{
		Request r[] = null;
		int i = 0;
		try 
		{
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS WHERE USERNAME = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			String safeQuery2 = "SELECT * FROM REQUESTS WHERE USERNAME = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ps2.setString(1, username);
			ResultSet rs2;
			if(rs.next())
			{
				r = new Request[rs.getInt(1)];
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					r[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					i++;
				}
			}
			else
			{
				return r;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
		}
		return r;
	}
	//don't search by reason wtf was I thinking
	//long id, String u, double a, String r, byte t, String d, byte s, String u2, String d2
	//possibly add greater than, less than... etc. to amount searching
	public Request[] getCustomSearchResults(Long id, String u, Double a, Byte t, String d, Byte s, String u2, String d2)
	{
		if((id == null) && ((u == null)||(u.equals(""))) && (a == null) && (t == null) && (d == null||d.equals("")) && (s == null) && (u2 == null||u2.equals("")) && (d2 == null||d2.equals("")))
		{
			return getRequests();
		}
		Request ret[] = null;
		int i = 0;
		try 
		{
			//could check id or amount 
			String safeQuery = "SELECT COUNT(*) FROM REQUESTS WHERE";
			String safeQuery2 = "SELECT * FROM REQUESTS WHERE";
			//creation of query lines, delete last " AND" using substring
			if(id != null)
			{
				safeQuery+= " REQUESTID = ? AND";
				safeQuery2+= " REQUESTID = ? AND";
			}
			if((u != null)&&!(u.equals("")))
			{
				safeQuery+= " USERNAME = ? AND";
				safeQuery2+= " USERNAME = ? AND";
			}
			//if we're going to do the greater than/less than, change this
			if(a != null)
			{
				safeQuery+= " AMOUNT = ? AND";
				safeQuery2+= " AMOUNT = ? AND";
			}
			if(t != null)
			{
				safeQuery+= " TYPE = ? AND";
				safeQuery2+= " TYPE = ? AND";
			}
			if((d != null)&&!(d.equals("")))
			{
				safeQuery+= " \"DATE\" = ? AND";
				safeQuery2+= " \"DATE\" = ? AND";
			}
			if(s!=null)
			{
				safeQuery+= " STATUS = ? AND";
				safeQuery2+= " STATUS = ? AND";
			}
			if((u2 != null)&&!(u2.equals("")))
			{
				safeQuery+= " USERNAMERESOLVE = ? AND";
				safeQuery2+= " USERNAMERESOLVE = ? AND";
			}
			if((d2 != null)&&!(d2.equals("")))
			{
				safeQuery+= " DATERESOLVE = ? AND";
				safeQuery2+= " DATERESOLVE = ? AND";
			}
			safeQuery = safeQuery.substring(0, safeQuery.length()-4);
			safeQuery2 = safeQuery2.substring(0, safeQuery2.length()-4);
			System.out.println(safeQuery);
			System.out.println(safeQuery2);
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			//creation of query lines, delete last " AND" using substring
			i = 1;
			if(id != null)
			{
				ps.setLong(i, id);
				ps2.setLong(i, id);
				i++;
			}
			if((u != null)&&!(u.equals("")))
			{
				ps.setString(i, u);
				ps2.setString(i, u);
				i++;
			}
			//if we're going to do the greater than/less than, change this
			if(a != null)
			{
				ps.setDouble(i, a);
				ps2.setDouble(i, a);
				i++;
			}
			if(t != null)
			{
				ps.setByte(i, t);
				ps2.setByte(i, t);
				i++;
			}
			if((d != null)&&!(d.equals("")))
			{
				System.out.println(d);
				ps.setString(i, d);
				ps2.setString(i, d);
				i++;
			}
			if(s!=null)
			{
				System.out.println(s);
				ps.setByte(i, s);
				ps2.setByte(i, s);
				i++;
			}
			if((u2 != null)&&!(u2.equals("")))
			{
				ps.setString(i, u2);
				ps2.setString(i, u2);
				i++;
			}
			if((d2 != null)&&!(d2.equals("")))
			{
				ps.setString(i, d2);
				ps2.setString(i, d2);
			}
			System.out.println(ps.toString());
			System.out.println(ps2.toString());
			ResultSet rs = ps.executeQuery();
			ResultSet rs2 = ps2.executeQuery();
			i = 0;
			if(rs.next())
			{
				ret = new Request[rs.getInt(1)];
				while(rs2.next())
				{
					//Request(long id, String u, double a, String r, byte t, String d, byte s)
					ret[i] = new Request(rs2.getLong(1),rs2.getString(2),rs2.getDouble(3),rs2.getString(4),rs2.getByte(5),rs2.getString(6),rs2.getByte(7),rs2.getString(8),rs2.getString(9));
					System.out.println("ret insert count: "+i);
					i++;
				}
			}
			else
			{
				return ret;
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return ret;
	}
	//only used by managers
	public byte changeRequestStatus(long id, String username, byte status)
	{
		//check status first, then change
		try 
		{
			String safeQuery = "SELECT * FROM REQUESTS WHERE REQUESTID = ?";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				if(rs.getByte(7) != 0)
				{
					//request already resolved
					return 1;
				}
			}
			else
			{
				//no request with that id found
				return 0;
			}
			String safeQuery2 = "UPDATE REQUESTS\nSET STATUS = ?, USERNAMERESOLVE = ?, DATERESOLVE = ? WHERE REQUESTID = ?";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ps2.setByte(1, status);
			ps2.setString(2, username);
			ps2.setString(3, java.time.LocalDate.now().toString());
			ps2.setLong(4,id);
			ps2.executeUpdate();
			return 2;
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			return 3;
		}
		//request successful
	}

	public boolean makeRequest(String u, double a, String r, byte t)
	{
		try 
		{
			//public Request(long id, String u, double a, String r, byte t, String d, byte s, String u2, String d2)
			String safeQuery = "INSERT INTO REQUESTS\nVALUES (?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			String safeQuery2 = "SELECT COALESCE(MAX(REQUESTID),0) FROM REQUESTS";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ResultSet rs2 = ps2.executeQuery();
			long i = 1;
			if(rs2.next())
			{
				i = rs2.getLong(1)+1;
			}
			ps.setLong(1, i);
			ps.setString(2, u);
			ps.setDouble(3, a);
			if(r==null)
			{
				ps.setString(4, "");
			}
			else
			{
				ps.setString(4, r);
			}
			ps.setByte(5, t);
			ps.setString(6, java.time.LocalDate.now().toString());
			ps.setByte(7, (byte)0);
			ps.setString(8, "");
			ps.setString(9, "");
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
}	
