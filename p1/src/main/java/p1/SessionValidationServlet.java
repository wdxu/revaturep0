package p1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SessionValidationServlet
 */
public class SessionValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionValidationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Got to validation");
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		if(request.getSession().getAttribute("username") != null && !request.getSession().getAttribute("username").equals(""))
		{
			System.out.println(request.getSession().getAttribute("username").toString());
			System.out.println(request.getSession().getAttribute("isManager").toString());
			if(request.getSession().getAttribute("isManager").equals((Integer)1))
			{
				response.getWriter().print("succM");
			}
			else
			{
				response.getWriter().print("succE");
			}
		}
		else
		{
			response.getWriter().print("fail");
		}
		response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
